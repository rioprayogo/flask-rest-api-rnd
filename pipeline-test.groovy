def ENV="env_test"

def APP_NAME = 'flask-rest-api'
def FACTORY = 'backend'

def CONFIG_PATH = "/home/jenkins/devops/config/" + FACTORY
def CONFIG_APP = CONFIG_PATH + "/" + ENV + "/" + APP_NAME + "/conf"

def VERSION_APP = ""
def WRAP_APP = CONFIG_PATH + "/" + ENV + "/" + APP_NAME
def KOANBA = "/home/rnd"


def JENKINS_IP = "103.139.192.82"

def SUBFOLDER = ""

def ROOT_WORKSPACE = '/var/lib/jenkins/workspace'
def FLASK_REST_API = ROOT_WORKSPACE + '/flask-rest-api'

pipeline{
    agent {
        label 'master'
    }
    parameters {
        booleanParam(name: 'build', defaultValue: false, description: '')
        booleanParam(name: 'Release-To-rio-idch', defaultValue: false, description: '')
    }
    
    stages {
        stage('Pull') {
            steps {
                git branch: 'master', credentialsId: 'rio-gitlab-idpass', url: 'https://gitlab.com/rioprayogo/flask-rest-api-rnd.git'
            }
        }
        stage('Building') {
            when {
                expression { params.build == true }
            }
            stages {
                stage('Build') {
                    steps {
                        script {
                           sh 'cp -rf ' + CONFIG_APP + '/*' FLASK_REST_API
                        }
                    }
                }          
            }
        }
        stage('realese') {
            agent {
                label 'rio-idch'
            }
            steps {
                script {
                    def isRelease = false
                    params.each{ 
                        k, 
                        v -> if(k.contains('Release-') && v) { 
                            isRelease = true
                            def paramData = k.split('-')
                            def appName = paramData[1];
                            def serverName = k.replaceAll('Release-' + appName + '-To-', '')
                            
                            node(serverName) {
                                def HOME = "/home/rnd/app/backend"
                                def HOME_APP = HOME + "/" + APP_NAME
                                sh '''cat > ~/.ssh/known_hosts'''
                                sh '''ssh-keyscan  -t ecdsa ''' + JENKINS_IP + ''' >> ~/.ssh/known_hosts'''
                                sh '''scp jenkins@''' + JENKINS_IP + ''':''' + WRAP_APP + '''/''' + ARTIFACT_APP + ''' /tmp'''
                                sh '''
                                mkdir -p ''' + HOME + ''' &&
                                rm -rf ''' + HOME_APP + ''' &&
                                mkdir ''' + HOME_APP + ''' &&
                                cd ''' + HOME_APP + '''
                                cp ''' + KOANBA + '''/''' + APP_NAME + '''-script.sh .
                                #========================================shutdown and start==================================
                                JENKINS_NODE_COOKIE=dontKillMe ./''' + APP_NAME + '''-script.sh
                                '''

                            }
                        }
                    }
                }
            }
        }
    }
}
