#!/bin/bash

fuser -k 5000/tcp
mkdir -p /home/rio/Documents/flask-rest-api/python-flask-restapi/log/
nohup python3 -m flask run --host=0.0.0.0 >> /home/rio/Documents/flask-rest-api/python-flask-restapi/log/python.log 2>&1 &
